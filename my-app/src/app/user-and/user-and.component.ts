import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-and',
  templateUrl: './user-and.component.html',
  styleUrls: ['./user-and.component.scss']
})
export class UserAndComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

class Repository {
}

let userRepository: Repository;

class InMemoryUserRepository extends Repository {
}

// @ts-ignore
beforeAll(() => {
  userRepository = new InMemoryUserRepository()
})

//2 ème

class CreateUser {
  constructor(userRepository: Repository) {

  }

}

let createUser: any;
// @ts-ignore
createUser = new CreateUser(userRepository).execute({
  firstname: 'John',
  lastname: 'Doe'
});

// @ts-ignore
expect(createUser.props.lastname).toEqual('Doe');







