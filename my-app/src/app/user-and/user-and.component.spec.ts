import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAndComponent } from './user-and.component';

describe('UserAndComponent', () => {
  let component: UserAndComponent;
  let fixture: ComponentFixture<UserAndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAndComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
