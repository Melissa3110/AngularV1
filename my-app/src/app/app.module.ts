import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
// @ts-ignore
import { Component } from "./Component";
import {ComponentDecoratorHandler} from "@angular/compiler-cli/src/ngtsc/annotations";
import {AppComponent} from "./component/app/app.component";
import { AboutComponent } from './about/about.component';
import { UserAndComponent } from './user-and/user-and.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  "declarations": [
    AppComponent,
    AboutComponent,
    UserAndComponent
  ],
  "imports": [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  "providers": [],
  "bootstrap": [AppComponent]
})
export class AppModule  { }

